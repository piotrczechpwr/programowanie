# Programowanie
[![made-with-python3](https://img.shields.io/badge/Made%20with-Python3-4B8BBE.svg)](https://www.laravel.com/)

## O repozytorium

Każdy katalog odpowiada 1 liście udostępnionej na <a href="https://eportal.pwr.edu.pl/course/view.php?id=61175">e-portalu</a>. <br><br>
Korzystam i sprawdzam swój kod przy pomocy: <br>
`Python 3.11.2 (main, Feb  8 2023, 14:49:24) [GCC 9.4.0] on linux`.

## Uruchomienie
```bash
chmod 775 ./listaX/main.py # gdzie X to nr listy
./listaX/main.py
```

lub

```bash
python3 ./listaX/main.py # gdzie X to nr listy
```

## Szczegółowe informacje

Więcej informacji na temat działania poszczególnych list znajduję się w dotyczących ich katalogach i README.
