import os
import qrcode
import cv2
from lib.file_utils import FileUtils

class QrUtils:

    def generate_qr(text_to_save: str, output_filename = 'qr.png'):
        """
        Generuje obraz kodu QR dla podanej wartości.

        :param text_to_save: Wartość do zapisania w kodzie QR.
        :param output_filename: Wartość do zapisania w kodzie QR.
        """
        if not isinstance(text_to_save, str):
            raise ValueError("Provided value to save in qr must be a string instance.")

        img = qrcode.make(text_to_save)

        if os.path.exists(output_filename):
            raise ValueError('Provided saving path already exists.')

        img.save(output_filename)

        return True        

    
    def read_qr(path: str):
        """
        Odczytuje zapisaną wartość w kodzie QR.

        :param path: Ścieżka do pliku.
        :return: Wartość do zapisania w kodzie QR.
        """
        abs_path = FileUtils.getAbsPath(path)

        if not (os.path.exists(abs_path) and os.path.isfile(abs_path)):
            raise ValueError("File does not exists.")

        value = None

        try:
            img = cv2.imread(path)
            detect = cv2.QRCodeDetector()
            value, _, __ = detect.detectAndDecode(img)
        except:
            raise ValueError("Incorrect qr-code file.")

        return value
    