import os

class FileUtils:
    def getAbsPath(file_path: str) -> str:
        """
        Zwraca ścieżkę w postaci absolutnej.

        :param file_path: ścieżka pliku wejściowego absolutna lub bezwględna
        """
        if not (os.path.isabs(file_path)):
            return os.path.abspath(file_path)

        return file_path
        

    def isImage(extension: str) -> bool:
        """
        Sprawdza czy rozszerzenie jest obsługiwane przez klasę.

        :param extension: rozszerzenie pliku poprzedzone kropką.
        """
        return (extension in ['.jpg', '.jpeg', '.png'])


    def is_utf8(data):
        "Returns a Unicode object on success, or None on failure"
        try:
            data.decode('utf-8')
            return True
        except UnicodeDecodeError:
            return False


    def changeEndOfLine(path: str):
        """
        Zmienia sposób kodowania znaku końca linii w podanym pliku z LF na CRLF i odwrotnie.

        :param path: ścieżka do pliku
        """
        absPath = FileUtils.getAbsPath(path)
        binary_windows_eol = b'\r\n'
        binary_unix_eol = b'\n'

        if not (os.path.exists(absPath) and  os.path.isfile(absPath)):
            raise ValueError("Provided path is incorrect.")

        content = None
        isWin = False

        with open(path, 'rb') as open_file:
            content = open_file.read()

            if not FileUtils.is_utf8(content):
                raise ValueError('Provided file is not utf-8 encoded.')

            if binary_windows_eol in content:
                isWin = True
            
        if (isWin):
            content = content.replace(binary_windows_eol, binary_unix_eol)
        else:
            content = content.replace(binary_unix_eol, binary_windows_eol)

        with open(absPath, 'wb') as open_file:
            open_file.write(content)

        return True