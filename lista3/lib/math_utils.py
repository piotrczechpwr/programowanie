class MathUtils:

    # Must be in the same order
    left_brackets = ['(', '[', '{', '<', '|']
    right_brackets = [')', ']', '}', '>', '|']

    def check_brackets(equation: str):
        """
        Sprawdza czy nawiasy są umieszczone w prawidłowy sposób w podanym równaniu

        :param equation: Równanie do sprawdzenia
        :return: Wartość boolean, True jeśli sposób zapisu jest poprawny.
        """
        if not isinstance(equation, str):
            raise ValueError("Provided value must be a string instance.")

        equation_brackets = []

        for letter in equation:
            if (
                (letter in MathUtils.left_brackets)
                and (
                    (letter != '|')
                    or (letter == '|' and (len(equation_brackets) == 0) or ((equation_brackets[-1] in MathUtils.left_brackets) and equation_brackets[-1] != "|"))
                )
            ):
                equation_brackets.append(letter)
            elif letter in MathUtils.right_brackets:
                bracket_index = MathUtils.right_brackets.index(letter)

                if(len(equation_brackets) < 1):
                    return False

                # Check bracket to close is avaible
                if (equation_brackets[-1] == MathUtils.left_brackets[bracket_index]):
                    equation_brackets.pop()
                else:
                    return False

        return True
    