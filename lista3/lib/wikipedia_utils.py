import requests
import webbrowser
from bs4 import BeautifulSoup

class WikipediaUtils:

    def get_random_article():
        """
        Mechanizm wyświetlania kolejnych artykułów w kółko do momentu wprowadzania przez użytkownika wiadomości "SELECT".
        """
        title, url = WikipediaUtils.get_random_article_url()
        print('> Provide (SELECT) in console whenever you want to open article. \n')

        print(title)
        while (input().lower() != "select"):
            title, url = WikipediaUtils.get_random_article_url()
            print(title)
            pass

        webbrowser.open(url)
        print(f'Opening {url}')

    def get_random_article_url():
        """
        Odczytuje z API losowy tytuł artykuły i jego url z wikipedi.

        :return: [nazwa artykułu, url artykułu]
        """
        r = requests.get('https://pl.wikipedia.org/wiki/Special:Random')

        if r.status_code != 200:
            raise RuntimeError("Wikipedia api dies (or internet).")

        soup = BeautifulSoup(r.text, 'html.parser')
        article_title = '"' + soup.title.string.replace(" – Wikipedia, wolna encyklopedia", "") + '"'

        return [article_title, r.url]
    
    