import os
import time
import shutil
from datetime import datetime

from lib.file_utils import FileUtils

class BackupUtils:

    def createBackupExtenstion(path, ext) -> str:
        """
        Tworzy kopię zapasową plików dla podanego katalogu o podanym rozszerzeniu

        :param path: ścieżka do pliku
        :param ext: przeszukiwane rozszerzenie
        """

        absPath = FileUtils.getAbsPath(path)

        if not (os.path.exists(absPath) and  os.path.isdir(absPath)):
            raise ValueError("Provided path is incorrect.")

        pathToSave = []

        for root, _, files in os.walk(absPath):
            for file in files:
                if file.endswith("." + ext):
                    filepath = os.path.join(root, file) 
                    days3 = (60 * 24 * 3)
                    now = time.time()

                    if (now - os.path.getmtime(filepath) < days3):
                        pathToSave.append(filepath)

        if (len(pathToSave) < 1):
            raise ValueError("Not found any file that matches provided requirements.")

        savepath = "Backup/copy-" + str(datetime.now().date())

        if (os.path.exists(savepath)):
            raise ValueError("Can't sile file to backup directory, becouse it already exists.")

        os.makedirs(savepath)

        for filepath in pathToSave:
            shutil.copy(filepath, savepath) 


        return True

