import os
from PyPDF2 import PdfWriter
from lib.file_utils import FileUtils

class PdfUtils:

    def merge_pdf(pdf_paths: list, output_filename: str = 'output.pdf'):
        """
        Łączy podane pliki pdf w jeden wspólny.

        :param pdf_paths: Ścieżka do plików pdf.
        :param output_filename: Wyjściowa nazwa pliku.
        """
        if (os.path.exists(output_filename)):
            raise ValueError("Saving path already exists.")

        pdf_abs_paths = []

        for filepath in pdf_paths:
            pdf_abs_paths.append(PdfUtils._getAbsPdfPath(filepath))

        output_pdf = PdfWriter()

        for pdf_abs_path in pdf_abs_paths:
            output_pdf.append(pdf_abs_path)

        output_pdf.write(output_filename)
        output_pdf.close()

        return True        
    

    def _getAbsPdfPath(path: str) -> str:
        """
        Otrzymaj poprawną nazwę pliku wejściowego i sprawdź czy istnieje.

        :param path: ścieżka relatywna/absolutna wejściowego PDF.
        :return: Absolutna ścieżka do pliku pdf.
        """
        pdf_path_abs = FileUtils.getAbsPath(path)

        if not (os.path.isfile(pdf_path_abs)):
            raise ValueError('Provided file does not exists.')
        
        _, file_ext = os.path.splitext(pdf_path_abs)
        if (file_ext != '.pdf'):
            raise ValueError('Provided file is not pdf.')
        
        return pdf_path_abs