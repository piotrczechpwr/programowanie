#!/usr/bin/env python3

import sys

import cli.create_backup
import cli.convert_eol
import cli.merge_pdfs
import cli.generate_qr
import cli.read_qr
import cli.check_brackets
import cli.get_wikipedia_article

def main():
    try:
        args = dict(zip(['_', 'util'], sys.argv))
        to_run = args['util']
        to_run = to_run.lower()
    except:
        to_run = None

    if (to_run == '--backup' or to_run == '-b'):
        cli.create_backup.main()
    elif (to_run == '--convert-eol' or to_run == '-eol'):
        cli.convert_eol.main()
    elif (to_run == '--merge-pdfs' or to_run == '-mp'):
        cli.merge_pdfs.main()
    elif (to_run == '--generate-qr' or to_run == '-gqr'):
        cli.generate_qr.main()
    elif (to_run == '--read-qr' or to_run == '-rqr'):
        cli.read_qr.main()
    elif (to_run == '--check-brackets' or to_run == '-c'):
        cli.check_brackets.main()
    elif (to_run == '--wikipedia-article' or to_run == '-a'):
        cli.get_wikipedia_article.main()
    else:
        print('Selected util is not recognized by program.')

if __name__ == "__main__":
    main()