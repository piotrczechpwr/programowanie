#!/usr/bin/env python3

import sys

from lib.pdf_utils import PdfUtils

def main():

    if (len(sys.argv) < 4):
        print("You have to provide atleast 2 paths for pdfs.")
        return
    
    PdfUtils.merge_pdf(sys.argv[2:])

    print('Done.')


if __name__ == "__main__":
    main()