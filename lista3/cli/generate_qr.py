#!/usr/bin/env python3

import sys

from lib.qr_utils import QrUtils

def main():

    if (len(sys.argv) < 3):
        print("You have to provided value for qr-code.")
        return

    value = sys.argv[2]

    QrUtils.generate_qr(value)

    print('Done.')


if __name__ == "__main__":
    main()