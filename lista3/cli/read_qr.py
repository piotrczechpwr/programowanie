#!/usr/bin/env python3

import sys

from lib.qr_utils import QrUtils

def main():

    if (len(sys.argv) < 3):
        print("You have to provided path for qr-code.")
        return

    path = sys.argv[2]

    print(QrUtils.read_qr(path))


if __name__ == "__main__":
    main()