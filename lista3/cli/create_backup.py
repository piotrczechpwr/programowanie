#!/usr/bin/env python3

import sys

from lib.backup_utils import BackupUtils

def main():

    if (len(sys.argv) < 4):
        print("You have to provide path for directory and searching extenstion.")
        return

    path = sys.argv[2]
    ext = sys.argv[3]

    BackupUtils.createBackupExtenstion(path, ext)

    print('Done.')


if __name__ == "__main__":
    main()