#!/usr/bin/env python3

import sys

from lib.file_utils import FileUtils

def main():

    if (len(sys.argv) < 3):
        print("You have to provide path for file.")
        return

    path = sys.argv[2]

    FileUtils.changeEndOfLine(path)

    print('Done.')


if __name__ == "__main__":
    main()