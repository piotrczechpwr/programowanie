#!/usr/bin/env python3

import sys

from lib.math_utils import MathUtils

def main():

    if (len(sys.argv) < 3):
        print("You have to provide an equation.")
        return

    equation = sys.argv[2]

    print(MathUtils.check_brackets(equation))


if __name__ == "__main__":
    main()