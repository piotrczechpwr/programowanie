#!/usr/bin/env python3

from lib.wikipedia_utils import WikipediaUtils

def main():
    WikipediaUtils.get_random_article()

if __name__ == "__main__":
    main()