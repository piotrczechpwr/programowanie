from scipy.integrate import odeint

class SeirModel:
    def __init__(self, N, s0, e0, i0, r0, beta, sigma, gamma):
        """
        Inicjalizuje obiekt modelu SEIR.

        :param N: Całkowita liczba populacji.
        :param s0: Liczba podatnych na zakażenie w stanie początkowym.
        :param e0: Liczba osób wystawionych na zakażenie w stanie początkowym.
        :param i0: Liczba osób zakażonych i chorujących w stanie początkowym.
        :param r0: Liczba osób, które wyzdrowiały lub zmarły w stanie początkowym.
        :param beta: Wskaźnik infekcjii (tempo jej rozprzestrzeniania się).
        :param sigma: Wskaźnik ikubacji (średni czas inkubacji to 1/σ).
        :param gamma: Wskaźnik wyzdrowień (jeśli średni czas infekcji wynosi D, wówczas γ = 1/D).
        """
        try:
            N = int(N)
            s0 = int(s0)
            e0 = int(e0)
            i0 = int(i0)
            r0 = int(r0)
            beta = float(beta)
            sigma = float(sigma)
            gamma = float(gamma)
        except:
            raise ValueError("Provided values should be correct numbers.")

        for n in [N, s0, e0, i0, r0, beta, sigma, gamma]:
            if n < 0:
                raise ValueError("All numbers should be positive.")

        if (N != (s0 + e0 + i0 + r0)):
            raise ValueError("N should be equal of s0, e0, i0 and r0 sum.")

        self.N = N
        self.s0 = s0
        self.e0 = e0
        self.i0 = i0
        self.r0 = r0
        self.beta = beta
        self.sigma = sigma
        self.gamma = gamma

    def get_seir_model(self, seir, t, N, beta, sigma, gamma):
        """
        Zwraca układ równań różniczkowych zwyczajnych dla modelu SEIR

        :param seir: to wartości (s0, e0, i0, r0) jako krotka:
            - s0 - Liczba podatnych na zakażenie w stanie początkowym.
            - e0 - Liczba osób wystawionych na zakażenie w stanie początkowym.
            - i0 - Liczba osób zakażonych i chorujących w stanie początkowym.
            - r0 - Liczba osób, które wyzdrowiały lub zmarły w stanie początkowym.
        :param t: Czas symulowania, potrzebny jedynie dla biblioteki scipy.
        :param N: Wielkość populacji.
        :param beta: Wskaźnik infekcjii (tempo jej rozprzestrzeniania się).
        :param sigma: Wskaźnik ikubacji (średni czas inkubacji to 1/σ).
        :param gamma: Wskaźnik wyzdrowień (jeśli średni czas infekcji wynosi D, wówczas γ = 1/D).
        """
        s, e, i, r = seir

        dSdt = (-beta * s * i) / N
        dEdt = ((beta * s * i ) / N) - (sigma * e)
        dIdt = (sigma * e) - (gamma * i)
        dRdt = gamma * i

        return dSdt, dEdt, dIdt, dRdt

    def solve_seir(self, t):
        """
        Rozwiązuje model SEIR dla podanego czasu.

        :param t: Czas symulacji w dniach
        :return: Wartośći S E I R dla każdego symulowanego dnia.
        """
        if not(isinstance(t, range)):
            raise ValueError("Incorrect value of simulation time.")

        for day in t:
            if not (day >= 0 and isinstance(day, int)):
                raise ValueError("Incorrect value of simulation time.")

        sol = odeint(self.get_seir_model, (self.s0, self.e0, self.i0, self.r0), t, args=(self.N, self.beta, self.sigma, self.gamma))

        return sol
