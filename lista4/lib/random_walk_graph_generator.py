import matplotlib.pyplot as plt
import imageio.v3 as iio
import networkx as nx
import random
import os

class RandomWalkGraphGenerator:
    def __init__(self, graph, title: str):
        """
        Inicjalizuje generator błądzenie losowe jednego agenta na losowym grafie

        :param graph: Graf na którym będą generowane spacery.
        :param title: Tytuł i nazwa folderu, w którym zapisany zostanie graf.
        """
        if not (isinstance(title, str)):
            raise ValueError('Title should be string.')

        self.graph = graph
        self.title = title

        self.current_point = None
        self.visited_points = []
        self.gif_steps = []

        self.display_graph_position = nx.random_layout(graph)

    def generate_random_walk(self, steps):
        """
        Generuje błądzenie losowe jednego agenta na podaną liczbę kroków.

        :param steps: Liczba kroków
        """
        if not (isinstance(steps, int) and steps > 0):
            raise ValueError('Incorrect value for steps.')

        self._initialize_walk()

        for step in range(steps):
            self._draw_graph_with_title(step)
            self._update_current_point()

        self._create_gif()

    def _initialize_walk(self):
        """
        Rezerwuje foldery i pliki dla generowania ścieżki.
        """
        start_point = random.choice(range(self.graph.number_of_nodes()))
        self.current_point = start_point
        self.visited_points.append(start_point)

        if (os.path.exists(f'assets/graphs/{self.title}/raw')):
            raise ValueError('Storage path for generating graph gif is occupied.')

        os.makedirs(f'assets/graphs/{self.title}/raw')

    def _draw_graph_with_title(self, step: int):
        """
        Tworzy graf aktualnego kroku i zapisuje jako obraz.

        :param step: Numer kroku.
        """
        plt.figure(figsize=(10, 10))
        plt.title(f'{self.title}, step {step + 1}')

        nx.draw(self.graph, self.display_graph_position, with_labels=True)
        nx.draw_networkx_nodes(self.graph, self.display_graph_position, nodelist=self.visited_points, node_color='orange')
        nx.draw_networkx_nodes(self.graph, self.display_graph_position, nodelist=[self.current_point], node_color='red')

        current_graph_path = f'assets/graphs/{self.title}/raw/{step}.png'
        self.gif_steps.append(current_graph_path)
        plt.savefig(current_graph_path)
        plt.close()

    def _update_current_point(self):
        """
        Aktualizuje aktulany punkt agenta. Ustawia na losowy.
        """
        neighbors = list(self.graph.neighbors(self.current_point))

        if len(neighbors) < 0:
            raise RuntimeError('Generated incorrect walk for graph.')

        self.current_point = random.choice(neighbors)
        self.visited_points.append(self.current_point)

    def _create_gif(self):
        """
        Tworzy plik GIF z błądzenia.
        """
        gif_steps = [iio.imread(img) for img in self.gif_steps]
        iio.imwrite(f'assets/graphs/{self.title}/walk.gif', gif_steps, extension='.gif', duration=500)

