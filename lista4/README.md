# Lista 2

## Uruchamianie
```bash
./seir.py [parametry] # Zadanie 1
```

lub 

```bash
python3.11 ./seir.py [parametry] # Zadanie 1
```

<br>

## Lista dostępnych komend i ich parametrów

### ./seir.py

```bash
python3.11 ./seir.py [parametry]
./seir.py [parametry]
```

Funkcjonalność z wskazanego podpunktu 1.<br>
Nie posiada argumentów wprowadzanych z konsoli. <br>
Wartości parametrów powinny zostać podane <br>
jako argumenty w linii poleceń w kolejności: `N, S0, E0, I0, R0, β, σ i γ`. <br>
Np.: `python3.11 seir.py 1000 999 1 0 0 1.34 0.19 0.34` <br>

Opis parametrów modelu SEIR: <br>
    - N - Całkowita liczba populacji. <br>
    - s0 - Liczba podatnych na zakażenie w stanie początkowym. <br>
    - e0 - Liczba osób wystawionych na zakażenie w stanie początkowym. <br>
    - i0 - Liczba osób zakażonych i chorujących w stanie początkowym. <br>
    - r0 - Liczba osób, które wyzdrowiały lub zmarły w stanie początkowym. <br>
    - beta - Wskaźnik infekcjii (tempo jej rozprzestrzeniania się). <br>
    - sigma - Wskaźnik ikubacji (średni czas inkubacji to 1/σ). <br>
    - gamma - Wskaźnik wyzdrowień (jeśli średni czas infekcji wynosi D, wówczas γ = 1/D). <br>

Za czas symulacji wskazana linia, którą możemy dowolnie zmieniać (wartość w dniach): <br>
```python
    simulation_days = range(150)

    # np.
    simulation_days = range(14)
    simulation_days = range(80)
    simulation_days = range(230)
```

### ./seir_wrapper.py

```bash
python3.11 ./seir_wrapper.py [parametry]
./seir_wrapper.py [parametry]
```

Funkcjonalność z wskazanego podpunktu 2.<br>
To samo co `seir.py`, natomiast z wygodniejszym korzystaniem z parametrów. <br>
Pomoc dostępna pod:
```python
# ./seir_wrapper.py --help

usage: seir_wrapper.py [-h] [-N N] [-S S] [-E E] [-I I] [-R R] [-beta BETA] [-sigma SIGMA] [-gamma GAMMA]
                       [-days DAYS]

options:
  -h, --help    show this help message and exit
  -N N          Całkowita liczba populacji.
  -S S          Liczba podatnych na zakażenie w stanie początkowym.
  -E E          Liczba osób wystawionych na zakażenie w stanie początkowym.
  -I I          Liczba osób zakażonych i chorujących w stanie początkowym.
  -R R          Liczba osób, które wyzdrowiały lub zmarły w stanie początkowym.
  -beta BETA    Wskaźnik infekcjii (tempo jej rozprzestrzeniania się).
  -sigma SIGMA  Wskaźnik ikubacji (średni czas inkubacji to 1/σ).
  -gamma GAMMA  Wskaźnik wyzdrowień (jeśli średni czas infekcji wynosi D, wówczas γ = 1/D).
  -days DAYS    Czas trwania symulacji w dniach
```

### ./graph.py

```bash
python3.11 ./graph.py
./graph.py
```

Funkcjonalność z wskazanego podpunktu 4.<br>
Generuje 3 gify z błądzenia agenta po grafie i zapisuje w `assets`.