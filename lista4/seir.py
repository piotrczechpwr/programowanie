#!/usr/bin/env python3.11
from scipy.integrate import odeint
from lib.seir_model import SeirModel
import matplotlib.pyplot as plt
import sys

def show_chart(simulation_days, solution, args):
    """
    Przedstawia model SEIR w czasie na wykresie

    :param simulation_days: Zakres dni, wartość osi-x.
    :param solution: wartości S E I R dla każdego symulowanego dnia.
    :param args: Wartości początkowe dla których odbyła się symulacja.
    """
    plt.axes().set_facecolor('#e5ecf6')

    plt.plot(simulation_days, solution[:, 0], 'mediumslateblue', label='S (Podatni)')
    plt.plot(simulation_days, solution[:, 1], 'tomato', label='E (Wystawieni)')
    plt.plot(simulation_days, solution[:, 2], 'mediumaquamarine', label='I (Chorzy)')
    plt.plot(simulation_days, solution[:, 3], 'mediumorchid', label='R (Po chorobie/zmarli)')

    plt.legend(loc='upper right', fontsize=12)
    plt.xlabel('dzień', fontsize=12)
    plt.ylabel('ilość osób', fontsize=12)
    plt.title(f'N={args[1]} s0={args[2]}, e0={args[3]}, i0={args[4]}, r0={args[5]}, β={args[6]}, σ={args[7]}, γ={args[8]}', fontsize=16)
    plt.grid(color='white')

    return plt.show()

def main():
    args = sys.argv
    if (len(args) != 9):
        raise SystemExit('Wrong number of params.')

    # Symulacja
    simulation_days = range(150)
    epidemic = SeirModel(args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8])
    solution = epidemic.solve_seir(simulation_days)

    # Wykres
    show_chart(simulation_days, solution, args)

if __name__ == "__main__":
    main()