#!/usr/bin/env python3.11
from scipy.integrate import odeint
from lib.seir_model import SeirModel
import matplotlib.pyplot as plt
import argparse

def show_chart(simulation_days, solution, args):
    """
    Przedstawia model SEIR w czasie na wykresie

    :param simulation_days: Zakres dni, wartość osi-x.
    :param solution: wartości S E I R dla każdego symulowanego dnia.
    :param args: Wartości początkowe dla których odbyła się symulacja.
    """
    plt.axes().set_facecolor('#e5ecf6')

    plt.plot(simulation_days, solution[:, 0], 'mediumslateblue', label='S (Podatni)')
    plt.plot(simulation_days, solution[:, 1], 'tomato', label='E (Wystawieni)')
    plt.plot(simulation_days, solution[:, 2], 'mediumaquamarine', label='I (Chorzy)')
    plt.plot(simulation_days, solution[:, 3], 'mediumorchid', label='R (Po chorobie/zmarli)')

    plt.legend(loc='upper right', fontsize=12)
    plt.xlabel('dzień', fontsize=12)
    plt.ylabel('ilość osób', fontsize=12)
    plt.title(f'N={args.N} s0={args.S}, e0={args.E}, i0={args.I}, r0={args.R}, β={args.beta}, σ={args.sigma}, γ={args.gamma}', fontsize=16)
    plt.grid(color='white')

    return plt.show()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-N', type=int, default=-1, help='Całkowita liczba populacji.')
    parser.add_argument('-S', type=int, default=-1, help='Liczba podatnych na zakażenie w stanie początkowym.')
    parser.add_argument('-E', type=int, default=-1, help='Liczba osób wystawionych na zakażenie w stanie początkowym.')
    parser.add_argument('-I', type=int, default=0, help='Liczba osób zakażonych i chorujących w stanie początkowym.')
    parser.add_argument('-R', type=int, default=0, help='Liczba osób, które wyzdrowiały lub zmarły w stanie początkowym.')
    parser.add_argument('-beta', type=float, default=1.34, help='Wskaźnik infekcjii (tempo jej rozprzestrzeniania się).')
    parser.add_argument('-sigma', type=float, default=0.19, help='Wskaźnik ikubacji (średni czas inkubacji to 1/σ).')
    parser.add_argument('-gamma', type=float, default=0.34, help='Wskaźnik wyzdrowień (jeśli średni czas infekcji wynosi D, wówczas γ = 1/D).')
    parser.add_argument('-days', type=int, default=150, help='Czas trwania symulacji w dniach')
    args = parser.parse_args()

    if (args.N < 0 and args.S < 0 and args.E < 0):
        args.S = 990
        args.E = 10
        args.N = args.S + args.E + args.I + args.R
    elif (args.S < 0 and args.E < 0):
        args.E = int(args.N * 0.1)
        args.S = args.N - args.E - args.I - args.R
    elif (args.N < 0 and args.S < 0):
        args.S = (args.E + args.I + args.R) * 10
        args.N = args.S + args.E + args.I + args.R
    elif (args.S < 0):
        args.S = args.N - args.E - args.I - args.R
    elif (args.N < 0 and args.E < 0):
        args.E = int((args.S + args.I + args.R) * 0.1)
        args.N = args.S + args.E + args.I + args.R
    elif (args.E < 0):
        args.E = args.N - args.S - args.I - args.R
    elif (args.N < 0):
        args.N = args.S + args.E + args.I + args.R

    # Symulacja
    simulation_days = range(args.days)
    epidemic = SeirModel(args.N, args.S, args.E, args.I, args.R, args.beta, args.sigma, args.gamma)
    solution = epidemic.solve_seir(simulation_days)

    # Wykres
    show_chart(simulation_days, solution, args)

if __name__ == "__main__":
    main()