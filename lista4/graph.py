#!/usr/bin/env python3.11
import networkx as nx
from lib.random_walk_graph_generator import RandomWalkGraphGenerator

def main():
    (RandomWalkGraphGenerator(nx.gnp_random_graph(20, 0.2), "Random walk on random graph")).generate_random_walk(20)
    (RandomWalkGraphGenerator(nx.barabasi_albert_graph(20, 4), "Random walk on Barabasi-Albert graph")).generate_random_walk(20)
    (RandomWalkGraphGenerator(nx.watts_strogatz_graph(20, 4, 0), "Random walk on Watts-Strogatz graph")).generate_random_walk(20) 
    print('Done.')


if __name__ == "__main__":
    main()