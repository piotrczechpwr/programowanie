#!/usr/bin/env python3.11
import pygame, sys
from lib.screens.main_menu import display_main_menu_screen

pygame.init()
pygame.mixer.init()

screen_width = 1280
screen_height = 720

SCREEN = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Dino run")

if __name__ == "__main__":
    display_main_menu_screen(SCREEN)