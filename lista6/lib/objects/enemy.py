import random, pygame, os

enemy_types = ["cactus", "rock", "bird"]

cactus_sprites = [
    pygame.transform.scale(pygame.image.load(os.path.join("assets/cactus/", "1.png")), (59, 96)),
    pygame.transform.scale(pygame.image.load(os.path.join("assets/cactus/", "2.png")), (45, 96)),
    pygame.transform.scale(pygame.image.load(os.path.join("assets/cactus/", "3.png")), (35, 96)),
    pygame.transform.scale(pygame.image.load(os.path.join("assets/cactus/", "4.png")), (33, 96)),
    pygame.transform.scale(pygame.image.load(os.path.join("assets/cactus/", "5.png")), (45, 96)),
    pygame.transform.scale(pygame.image.load(os.path.join("assets/cactus/", "6.png")), (42, 96)),
]

rock_sprites = [
    pygame.transform.scale(pygame.image.load(os.path.join("assets/rock/", "1.png")), (96, 96)),
    pygame.transform.scale(pygame.image.load(os.path.join("assets/rock/", "2.png")), (96, 96)),
    pygame.transform.scale(pygame.image.load(os.path.join("assets/rock/", "3.png")), (96, 88)),
]

bird_sprites = [
    pygame.transform.scale(pygame.image.load(os.path.join("assets/bird/", "1.png")), (62, 42)),
    pygame.transform.scale(pygame.image.load(os.path.join("assets/bird/", "2.png")), (62, 20)),
    pygame.transform.scale(pygame.image.load(os.path.join("assets/bird/", "3.png")), (62, 34)),
]

enemy_sprites = {
    "cactus": cactus_sprites,
    "rock": rock_sprites,
    "bird": bird_sprites,
}

class Enemy:
    def __init__(self):
        """
        Utwórz losową przeszkodę
        """
        self.type = random.choice(enemy_types)
        self.image = enemy_sprites[self.type]
        self.image = random.choice(self.image)
        self.rect = self.image.get_rect()

        if (self.type == "cactus" or self.type == "rock"):
            self.rect.y = 420
        else:
            self.rect.y = 405

        self.rect.x = 1200
        self.step_index = 0

    def update(self, game_speed):
        """
        Obsłuż ruch wroga w aktualnej klatce

        :param game_speed: Prękość przesuwania przeszkody
        """
        self.rect.x -= game_speed

        # Slower anim bird
        if self.type == "bird":
            self.step_index += 1

            if self.step_index >= 15:
                self.step_index = 0
            
            self.image = bird_sprites[self.step_index // 5]

        # Dead
        if self.rect.x < -self.rect.width:
            return True

    def draw(self, screen):
        """
        Wyświetl wroga na ekranie, tak żeby zawsze była to ta sama pozycja względem wysokości obrazka

        :param screen: Aktualna scena
        """
        anchor = self.image.get_rect().bottom 
        screen.blit(self.image, (self.rect.x, self.rect.bottom - anchor))