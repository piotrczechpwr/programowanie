import pygame
import os

class Player:
    def __init__(self):
        """
        Obiekt gracza dinozaura
        """
        self.crouch_anim = [
            self._load_image("crouch", "1", w=64, h=45),
            self._load_image("crouch", "2", w=64, h=45),
        ]
        self.run_anim = [
            self._load_image("run", "1"),
            self._load_image("run", "2"),
        ]
        self.jump_anim = [
            self._load_image("jump", "1"),
            self._load_image("jump", "2"),
        ]

        self.state = "run"

        self.hit_sound = pygame.mixer.Sound("assets/player/dino_sad.mp3")

        self.step_index = 0
        self.height_velocity = 8
        self.lives = 3
        self.image = self.run_anim[0]
        self.rect = self.image.get_rect()
        self.rect.x = 100
        self.rect.y = 450
    
    def _load_image(self, path, filename, w=64, h=64):
        """
        Wczytaj poszczególne obrazy do animacji dinozaura odpowiednio przeskalowane

        :param path: Ścieżka po assets/player/
        :param filename: Nazwa pliku, koncówka .png dodawana automatycznie
        :param w: Oczekiwana szerokość (domyślnie 64)
        :param h: Oczekiwana wysokość (domyślnie 64)
        """
        return pygame.transform.scale(pygame.image.load(os.path.join(f"assets/player/{path}", filename + ".png")), (w, h)) 

    def _get_anim_frame(self, frames):
        """
        Wybierz i odczytaj odpowiednia klatkę animacji

        :param frames: Klatki animacji
        """
        return frames[self.step_index // 5]

    def update(self, userInput):
        """
        Sprawdź czy użytkownik chce zmienic ruch gracza + domyślne zmiany podczas gry

        :param userInput: Akcja od użytkownika
        """
        if self.state == "crouch":
            self.crouch()
        elif self.state == "jump":
            self.jump()
        else:
            self.run()

        # Slower anim
        self.step_index += 1

        if self.step_index >= 10:
            self.step_index = 0

        if self.state == "jump":
            return

        if userInput[pygame.K_UP]:
            self.state = "jump"
            self.height_velocity = 8
        elif userInput[pygame.K_DOWN]:
            self.state = "crouch"
        else:
            self.state = "run"

    def crouch(self):
        """
        Stan gracza jeśli kuca
        """
        self.image = self._get_anim_frame(self.crouch_anim)

    def run(self):
        """
        Stan gracza jeśli biega
        """
        self.image = self._get_anim_frame(self.run_anim)

    def jump(self):
        """
        Stan gracza jeśli skacze
        """
        self.image = self._get_anim_frame(self.jump_anim)
        velocity = 4

        if self.state == "jump":
            self.rect.y -= self.height_velocity * 4
            self.height_velocity -= velocity * 0.1

        # end jump
        if self.rect.y > 450:
            self.state = "run"
            self.rect.y = 450
            self.height_velocity = velocity

    def display(self, screen):
        """
        Wyświetl gracza na ekranie, tak żeby zawsze była to ta sama pozycja względem wysokości obrazka

        :param screen: Aktualna scena
        """
        anchor = self.image.get_rect().bottom 
        screen.blit(self.image, (self.rect.x, self.rect.bottom - anchor))

    def hit(self):
        """
        Obsłuż uderzenie dinozaura
        """
        self.lives -= 1
        self.hit_sound.play()
