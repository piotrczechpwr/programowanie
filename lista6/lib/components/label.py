from lib.game_manager import get_font

class Label():
	def __init__(self, text, pos, size=45, color=(255, 255, 255)):
		"""
		Utwórz napis na ekranie.

		:param text: Zawartość
		:param pos: Pozycja
		:param size: Wielkość (domyślnie 45)
		:param color: Kolor (domyślnie biały)
        """
		self.text_string = get_font(size).render(text, True, color)
		self.text_rect = self.text_string.get_rect(center=((pos)))
    
	def update(self, screen):
		"""
		Wyświetlaj napis na ekranie
		"""
		screen.blit(self.text_string, self.text_rect)