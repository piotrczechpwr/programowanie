from lib.game_manager import get_font

class Button():
	def __init__(self, text, pos, size=50, base_color=(255, 255, 255), hovering_color=(200, 200, 200)):
		"""
		Utwórz przycisk na ekranie.

		:param text: Zawartość
		:param pos: Pozycja
		:param size: Wielkość (domyślnie 50)
		:param base_color: Kolor domyślny (domyślnie biały)
		:param hovering_color: Kolor po najechaniu myszą (domyślnie lekko szary)
        """
		self.x_pos = pos[0]
		self.y_pos = pos[1]
		self.font = get_font(size)
		self.base_color, self.hovering_color = base_color, hovering_color
		self.text_input = text
		self.text = self.font.render(self.text_input, True, self.base_color)
		self.rect = self.text.get_rect(center=(self.x_pos, self.y_pos))
		self.text_rect = self.text.get_rect(center=(self.x_pos, self.y_pos))

	def update(self, screen):
		"""
		Wyświetlaj przycisk na ekranie
		"""
		screen.blit(self.text, self.text_rect)

	def checkForInput(self, position):
		"""
		Sprawdź, czy przycisk zostal naciśnięty

		:param position: Aktualna pozycja myszy
		"""
		if position[0] in range(self.rect.left, self.rect.right) and position[1] in range(self.rect.top, self.rect.bottom):
			return True
		return False

	def changeColor(self, position):
		"""
		Zmień kolor przycisku

		:param position: Aktualna pozycja myszy
		"""
		if position[0] in range(self.rect.left, self.rect.right) and position[1] in range(self.rect.top, self.rect.bottom):
			self.text = self.font.render(self.text_input, True, self.hovering_color)
		else:
			self.text = self.font.render(self.text_input, True, self.base_color)