import pygame, sys

def get_font(size):
    """
    Odczytaj domyślną dla gry czcionke o podanej wielkości.

    :param size: Podana wielkość
    """
    return pygame.font.Font(None, size)

def quit_game():
    """
    Wyjdź z gry
    """
    pygame.quit()
    sys.exit()

def listen_buttons(buttons, mouse_pos, screen):
    """
    Utwórz listener do wszystkich podanych przycisków na akcje "hover"

    :param buttons: Lista przycisków
    :param mouse_pos: Aktualna pozycja myszy
    :param screen: Aktualna scena
    """
    for button in buttons.values():
        button.changeColor(mouse_pos)
        button.update(screen)