import os

def write_score(user_score):
    """
    Sprawdź czy wynik jest topowy, jeśli tak to zapisz

    :param user_score: Wynik
    """
    best_scores = read_best_scores()
    
    best_scores.append(user_score)
    best_scores.sort(reverse=True)

    with open('./assets/scores.csv', 'w') as f:
        for i in range(min(len(best_scores), 3)):
            best_score = str(best_scores[i])
            f.write(best_score + '\n')

def read_best_scores():
    """
    Odczytaj najlepsze wyniki
    """
    if (os.path.exists('./assets') and (not os.path.isdir('./assets'))):
        raise SystemError('Assets directory is incorrect.')

    if not (os.path.exists('./assets/scores.csv')):
        with open('./assets/scores.csv', 'w') as f:
            f.write('')

    best_scores = []

    with open('./assets/scores.csv', 'r') as f:
        content = f.readlines()
        if len(content) > 0:
            for score_row in content:
                best_scores.append(int(score_row))

    return best_scores[0:3]
