import pygame, sys, os, random
import lib.screens.dead as dead
from lib.components.label import Label
from lib.objects.player import Player
from lib.objects.enemy import Enemy

fps = 60

def display_play_screen(screen):
    """
    Scena: Gra
    """
    player = Player()
    score = 0

    enemies = []
    enemy_spawn_timer = 0
    enemy_spawn_delay = 3000

    sun = pygame.transform.scale(pygame.image.load(os.path.join(f"assets/bg", "sun.png")), (128, 128)) 
    middle_bg = pygame.transform.scale(pygame.image.load(os.path.join(f"assets/bg", "middle_bg.png")), (1280, 150)) 

    while True:
        screen.fill((173, 163, 157), rect=pygame.Rect(0, 0, 1280, 500))
        screen.fill((199, 194, 189), rect=pygame.Rect(0, 500, 1280, 720))
        screen.blit(middle_bg, (0, 350)) 
        screen.blit(sun, (800, 100)) 

        Label(f"Punkty {round(score)}", (150, 50)).update(screen)
        Label(f"Życia {player.lives}", (1100, 50)).update(screen)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        if player.lives > 0:
            clock = pygame.time.Clock()

            enemy_spawn_timer += (1 / fps) * 1000

            player.display(screen)
            player.update(pygame.key.get_pressed())

            if enemy_spawn_timer >= enemy_spawn_delay:
                enemies.append(Enemy())
                enemy_spawn_timer = 0
                enemy_spawn_delay = random.randint(500, 2000)

            min_speed = 8
            max_speed = 20
            speed = min_speed + ((max_speed - min_speed) * (score / 500))

            for enemy in enemies:
                enemy.update(speed)
                is_dead = enemy.draw(screen)

                if (is_dead):
                    enemies.remove(enemy)
                    score += 5
                
                if player.rect.colliderect(enemy.rect):
                    player.hit()
                    enemies = []
                    screen.fill((255, 0, 0), rect=pygame.Rect(0, 0, 1280, 720))

            clock.tick(fps)
            score += 1 / fps
            pygame.display.update()
        else:
            dead.display_dead_screen(screen, round(score))
