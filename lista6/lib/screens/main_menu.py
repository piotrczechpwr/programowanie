import pygame, sys
from lib.game_manager import quit_game, listen_buttons
from lib.components.button import Button
from lib.components.label import Label

from lib.screens.play import display_play_screen
from lib.screens.rules import display_rules_screen
from lib.screens.top_scores import display_top_scores_screen
from lib.screens.about import display_about_screen

def display_main_menu_screen(screen):
    """
    Scena: Menu główne
    """
    while True:
        screen.fill((30, 30, 30))

        mouse_pos = pygame.mouse.get_pos()
        buttons = {}

        Label("Menu główne", (640, 150), size=75).update(screen)

        buttons["play"] = Button("Start gry", (640, 250))
        buttons["rules"] = Button("Zasady gry", (640, 325))
        buttons["top_scores"] = Button("Najlepsze wyniki", (640, 400))
        buttons["author"] = Button("O autorze", (640, 475))
        buttons["quit"] = Button("Zakończenie programu", (640, 550), hovering_color=(255, 0, 0))

        listen_buttons(buttons, mouse_pos, screen)
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if buttons["play"].checkForInput(mouse_pos):
                    display_play_screen(screen)
                elif buttons["rules"].checkForInput(mouse_pos):
                    display_rules_screen(screen)
                elif buttons["top_scores"].checkForInput(mouse_pos):
                    display_top_scores_screen(screen)
                elif buttons["author"].checkForInput(mouse_pos):
                    display_about_screen(screen)
                elif buttons["quit"].checkForInput(mouse_pos):
                    quit_game()

        pygame.display.update()