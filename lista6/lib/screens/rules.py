import pygame, sys
from lib.components.label import Label
from lib.game_manager import listen_buttons
import lib.screens.main_menu as main_menu
from lib.components.button import Button

def display_rules_screen(screen):
    """
    Scena: Zasady
    """
    while True:
        screen.fill((30, 30, 30))

        mouse_pos = pygame.mouse.get_pos()
        buttons = {}

        Label("Zasady gry", (640, 150), size=75).update(screen)
        Label("W grze wcielamy się w dinozaura uciekającego przed światową katastrofą.", (640, 225), size=25).update(screen)
        Label("Unikaj przeszkód i przebiegnij jak najdalej, masz 3 życia.", (640, 250), size=25).update(screen)

        Label("Sterowanie:", (640, 350), size=75).update(screen)
        Label("SKOK - Strzałka w górę:", (640, 425), size=25).update(screen)
        Label("KUCANIE - Strzałka w dół:", (640, 450), size=25).update(screen)

        buttons["back"] = Button("Menu główne", (640, 550))

        listen_buttons(buttons, mouse_pos, screen)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if buttons["back"].checkForInput(mouse_pos):
                    main_menu.display_main_menu_screen(screen)

        pygame.display.update()
