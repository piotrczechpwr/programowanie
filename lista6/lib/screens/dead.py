import pygame, os, sys
import lib.screens.main_menu as main_menu
from lib.components.label import Label
from lib.components.button import Button
from lib.game_manager import listen_buttons
from lib.utils.scores import write_score

def display_dead_screen(screen, score):
    """
    Scena: Ekran pośmiertny, niewyświetlany w menu głównym
    """
    write_score(score)

    while True:
        mouse_pos = pygame.mouse.get_pos()
        buttons = {}

        sun = pygame.transform.scale(pygame.image.load(os.path.join(f"assets/bg", "sun.png")), (128, 128)) 
        middle_bg = pygame.transform.scale(pygame.image.load(os.path.join(f"assets/bg", "middle_bg.png")), (1280, 150)) 

        screen.fill((173, 163, 157), rect=pygame.Rect(0, 0, 1280, 500))
        screen.fill((199, 194, 189), rect=pygame.Rect(0, 500, 1280, 720))
        screen.blit(middle_bg, (0, 350)) 
        screen.blit(sun, (800, 100)) 

        Label(f"Zdobyte punkty {score}", (640, 150), size=75).update(screen)

        buttons["back"] = Button("Menu główne", (640, 550))

        listen_buttons(buttons, mouse_pos, screen)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if buttons["back"].checkForInput(mouse_pos):
                    main_menu.display_main_menu_screen(screen)

        pygame.display.update()
