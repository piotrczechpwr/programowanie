import pygame, sys
import lib.screens.main_menu as main_menu
from lib.components.label import Label
from lib.game_manager import listen_buttons
from lib.components.button import Button
from lib.utils.scores import read_best_scores

def display_top_scores_screen(screen):
    """
    Scena: Najlepsze wyniki
    """
    while True:
        screen.fill((30, 30, 30))

        mouse_pos = pygame.mouse.get_pos()
        buttons = {}

        Label("Najlepsze wyniki:", (640, 150), size=75).update(screen)

        best_scores = read_best_scores()
        for i in range(3):
            best_score = "Brak"

            if (len(best_scores) > i):
                best_score = best_scores[i]

            Label(f"{i + 1} miejsce: {best_score}", (640, 200 + (25 * i)), size=25).update(screen)

        buttons["back"] = Button("Menu główne", (640, 550))

        listen_buttons(buttons, mouse_pos, screen)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if buttons["back"].checkForInput(mouse_pos):
                    main_menu.display_main_menu_screen(screen)

        pygame.display.update()
