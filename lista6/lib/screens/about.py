import pygame, sys
from lib.components.label import Label
from lib.game_manager import listen_buttons
import lib.screens.main_menu as main_menu
from lib.components.button import Button

def display_about_screen(screen):
    """
    Scena: O autorze
    """
    while True:
        screen.fill((30, 30, 30))

        mouse_pos = pygame.mouse.get_pos()
        buttons = {}

        Label("O autorze", (640, 150), size=75).update(screen)
        Label("Piotr Czech", (640, 225), size=25).update(screen)
        Label("276014", (640, 250), size=25).update(screen)

        buttons["back"] = Button("Menu główne", (640, 550))

        listen_buttons(buttons, mouse_pos, screen)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if buttons["back"].checkForInput(mouse_pos):
                    main_menu.display_main_menu_screen(screen)

        pygame.display.update()
