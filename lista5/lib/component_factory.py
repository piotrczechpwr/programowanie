from tkinter import *
from tkinter import ttk

class ComponentFactory:
    def __init__(self, window):
        """
        Fabryka komponentów w oparciu o bibliotekę python tkinter.
        Wzorzec projektowy - Fabryka.

        :param window: Aktualna scena/okno tkinter.
        """
        self.parent = window
        self.parent.option_add("*TCombobox*Listbox*Font", ("Arial", 12))

    def build_label(self, text, **k):
        """
        Tworzy label/tekst/napis

        :param text: Tekst do wyświetlenia.
        :param **k: Dowolne argumenty spełniające warunki konstruktora tkinter Label().
        """
        return Label(self.parent, text=text, font=("Arial", 14), **k)

    def build_entry(self, **k):
        """
        Tworzy pole do wprowadzania tekstu

        :param **k: Dowolne argumenty spełniające warunki konstruktora tkinter Entry().
        """
        return Entry(self.parent, width=36, font=("Arial", 12), **k)

    def build_button(self, text, command, **k):
        """
        Tworzy przycisk

        :param text: Tekst do wyświetlenia.
        :param command: Funkcja uruchamiana po kliknięciu.
        :param **k: Dowolne argumenty spełniające warunki konstruktora tkinter Button().
        """
        return Button(self.parent, text=text, command=command, font=("Arial", 14), width=30, height=2, **k)

    def build_dropdown(self, values, initial_index, **k):
        """
        Tworzy rozwijany select.

        :param values: Lista wartości.
        :param initial_index: Domyślnie zaznaczona wartość.
        :param **k: Dowolne argumenty spełniające warunki konstruktora tkinter Combobox().
        """
        dropdown = ttk.Combobox(self.parent, values=values, font=("Arial", 12), state='readonly', width=35, height=20, **k)
        dropdown.current(initial_index)
        return dropdown

    def build_spacer(self, row = 1, pady = 15):
        """
        Tworzy przerwę

        :param row: Nr wiersza do wstawienia przerywnika.
        :param pady: Wielkość marginesu.
        """
        spacer = self.build_label("").grid(row=row, pady=pady)
        return spacer