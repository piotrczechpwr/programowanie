import requests

from lib.currency import Currency

class NBPApi:
    def __init__(self):
        """
        Api NPB.
        Dokumentacja: http://api.nbp.pl/
        """
        self.base_domain = 'https://api.nbp.pl/api/'

    def todayExchangeRates(self, table_name = 'A'):
        """
        Odczyt dzisiejszego kursu walut z wybranej tabeli. Domyślnie A.

        :param table_name: Nazwa tabeli do odczytu danych (enum: A, B, C).
        """
        if not (table_name == 'A' or table_name == 'B' or table_name == 'C'):
            raise ValueError('Incorrect table name.')
        
        response = requests.get(f'{self.base_domain}exchangerates/tables/{table_name}/today?format=json')

        if (response.status_code != 200):
            raise SystemError('Incorrect request.')
        
        response_json = response.json()

        effective_date = response_json[0]['effectiveDate']
        rates_raw = response_json[0]['rates']
        rates = [Currency(c['currency'], c['code'], c['mid']) for c in rates_raw]

        return effective_date, rates
        
    def todayGoldRate(self):
        """
        Odczyt dzisiejszego kursu złota.
        """
        response = requests.get(f'{self.base_domain}cenyzlota/today')

        if (response.status_code != 200):
            raise SystemError('Incorrect request.')
        
        response_json = response.json()

        effective_date = response_json[0]['data']
        gold_rate = response_json[0]['cena']

        gold = Currency('złoto', 'AU', gold_rate)

        return effective_date, gold