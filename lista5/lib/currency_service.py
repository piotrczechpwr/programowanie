import os
from datetime import date
from lib.currency import Currency
from lib.nbp_api import NBPApi

class CurrencyService:

    def __init__(self):
        """
        Serwis do współpracy z walutami. Umożliwia między innymi odczyt aktualnego kursu.
        """
        if (os.path.exists('./assets') and (not os.path.isdir('./assets'))):
            raise SystemError('Assets directory is incorrect.')
            
        if not (os.path.exists('./assets')):
            os.makedirs('./assets')

        if not (os.path.exists('./assets/exchange_rates.csv')):
            with open('./assets/exchange_rates.csv', 'w') as f:
                f.write('')

        self.rates = []
        
        with open('./assets/exchange_rates.csv', 'r') as f:
            content = f.readlines()
            if len(content) > 0:
                effective_date = content[0].strip()
                if (effective_date == str(date.today())):
                    self.rates = []
                    for line in content[1:]:
                        self.rates.append(Currency(*line.strip().split(';')))
                    print('Currency read from file.')
                    return
        
        self._updateRates()

    def getExchangeRates(self):
        """
        Zwraca aktualną wielkość kursów.
        """
        return self.rates

    def _updateRates(self):
        """
        Aktualizuje wielkość kursów przy pomocy api NBP.
        Zapisuje waluty z tabeli NPB-A, PLN i złoto
        """
        effective_date, money_rates = NBPApi().todayExchangeRates()
        _, goldRate = NBPApi().todayGoldRate()
        money_rates.append(Currency('polski złoty', 'PLN', 1.0))

        rates = [*money_rates, goldRate]

        with open('./assets/exchange_rates.csv', 'w') as f:
            f.write(effective_date + '\n')
            [f.write(str(rate) + '\n') for rate in rates]
            print('Currency read from API.')
        
        self.rates = rates

