from tkinter import *
from tkinter import messagebox

from lib.component_factory import ComponentFactory
from lib.currency_service import CurrencyService

class AppManager:
    def __init__(self):
        """
        Inicjalizuje konfigurację aplikację
        """
        self.window = Tk()

        self.currency_from_select = None
        self.currency_to_select = None

        self.currency_amount = DoubleVar()
        self.result_amount = DoubleVar()

        self.currencies = CurrencyService().getExchangeRates()

        self.create_main_scene()

    def run(self):
        """
        Uruchamia przeglądarke w trybie GUI.
        """
        self.window.mainloop()

    def create_main_scene(self):
        """
        Tworzy layout i podstawowe obiekty do głównej sceny.
        """
        self.window.title("Calculator")

        component_factory = ComponentFactory(self.window)
        options = [f'{c.name} ({c.code})' for c in self.currencies]

        label1 = component_factory.build_label("Wybierz walutę (z)")
        label1.grid(row=0, column=0, padx=20, pady=10)

        dropdown1 = component_factory.build_dropdown(options, 0)
        dropdown1.grid(row=1, column=0, padx=20)
        self.currency_from_select = dropdown1

        label2 = component_factory.build_label("Wybierz walutę (na)")
        label2.grid(row=0, column=1, padx=20, pady=10)

        dropdown2 = component_factory.build_dropdown(options, 0)
        dropdown2.grid(row=1, column=1, padx=20)
        self.currency_to_select = dropdown2

        component_factory.build_spacer(row = 2)

        label3 = component_factory.build_label("Wprowadź wartość")
        label3.grid(row=3, column=0, padx=20, pady=10)

        entry1 = component_factory.build_entry(textvariable=self.currency_amount)
        entry1.grid(row=4, column=0, padx=20)

        label4 = component_factory.build_label("Wynik")
        label4.grid(row=3, column=1, padx=20, pady=10)

        label5 = component_factory.build_label("", textvariable=self.result_amount)
        label5.grid(row=4, column=1, padx=20)
        self.result_label = label5

        component_factory.build_spacer(row = 5)

        button_quit = component_factory.build_button("Wyjdź", self.window.quit)
        button_quit.grid(row=6, column=0, padx=20, pady=10)

        button_calc = component_factory.build_button("Przelicz", self.calculate_result)
        button_calc.grid(row=6, column=1, padx=20, pady=10)

    def calculate_result(self):
        """
        Oblicza wynik dla użytkownika na podstawie zaznaczonych walut i wprowadzonej wartości.
        """
        amount = 0

        try:
            amount = self.currency_amount.get()

            if not isinstance(amount, (float, int)) or amount <= 0:
                messagebox.showerror(
                    title='Krytyczny błąd systemu!',
                    message='Pole "Wprowadź wartość" zostało niepoprawnie uzupełnione. Upewnij się, że została wprowadzona dodatnia liczba wymierna.'
                )
                return
        except:
            messagebox.showerror(
                title='Krytyczny błąd systemu!',
                message='Pole "Wprowadź wartość" zostało niepoprawnie uzupełnione. Upewnij się, że została wprowadzona dodatnia liczba wymierna.'
            )
            return

        try:
            currency_from = self.currencies[self.currency_from_select.current()]
            currency_to = self.currencies[self.currency_to_select.current()]
        except:
            raise ValueError('Selected incorrect currency.')

        pln_value = amount * currency_from.rate
        result_value = round(pln_value / currency_to.rate, 2)

        self.result_amount.set(result_value)
        