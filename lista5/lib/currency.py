class Currency:
    def __init__(self, name, code, rate):
        """
        Obiekt waluty

        :param name: Nazwa waluty.
        :param code: Kod waluty.
        :param rate: Przelicznik waluty, ilość PLN za otrzymanie 1 wielkości waluty.
        """
        self.name = name
        self.code = code
        self.rate = float(rate)

    def __str__(self):
        """
        Formatuje walutę do przystępnej dla zapisu CSV formy.
        """
        return f'{self.name};{self.code};{self.rate}'