#!/usr/bin/env python3.11

from lib.app_manager import AppManager

def main():
    """
    Aplikacja kalkulatora walut, uruchamia główny wątek.
    """
    app = AppManager()
    app.run()

if __name__ == "__main__":
    main()
