#!/usr/bin/env python3

import sys

import cli.generate_password
import cli.generate_thumbnail
import cli.create_zip
import cli.divide_pdf
import cli.generate_watermark
import cli.generate_calcs

def main():
    try:
        args = dict(zip(['_', 'util'], sys.argv))
        to_run = args['util']
        to_run = to_run.lower()
    except:
        to_run = None

    if (to_run == 'password' or to_run == 'p'):
        cli.generate_password.main()
    elif (to_run == 'thumbnail' or to_run == 't'):
        cli.generate_thumbnail.main()
    elif (to_run == 'zip' or to_run == 'z'):
        cli.create_zip.main()
    elif (to_run == 'pdf' or to_run == 'd'):
        cli.divide_pdf.main()
    elif (to_run == 'watermark_text' or to_run == 'wt'):
        cli.generate_watermark.byText()
    elif (to_run == 'watermark_image' or to_run == 'wi'):
        cli.generate_watermark.byImage()
    elif (to_run == 'maths' or to_run == 'm'):
        cli.generate_calcs.main()
    else:
        print('Selected util is not recognized by program.')

if __name__ == "__main__":
    main()