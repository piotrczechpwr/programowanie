#!/usr/bin/env python3

import sys

from lib.zip_utils import ZipUtils

def main():

    if (len(sys.argv) < 4):
        print("You have to provide atleast zip save path and 1 file/directory.")
        return

    zip_save_path = sys.argv[2]

    zip_path = ZipUtils.createZip(zip_save_path, sys.argv[3:])

    print(f'Done. Zip created at {zip_path}')


if __name__ == "__main__":
    main()