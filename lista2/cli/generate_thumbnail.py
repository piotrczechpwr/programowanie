#!/usr/bin/env python3

import sys

from lib.image_utils import ImageUtils

def main():

    if (len(sys.argv) < 3):
        print("You have to provide path to an image.")
        return

    img_path = sys.argv[2]
    ImageUtils.createThumbnail(file_path=img_path, thumbnail_height=256, thumbnail_width=256)

    print('Done.')

if __name__ == "__main__":
    main()