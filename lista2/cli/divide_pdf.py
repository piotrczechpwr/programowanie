#!/usr/bin/env python3

import sys

from lib.pdf_utils import PdfUtils

def main():

    if (len(sys.argv) < 4):
        print("You have to provide atleast pdf read path and 1 number of expected pages.")
        return

    pdf_path = sys.argv[2]

    PdfUtils.divide_pdf(pdf_path, sys.argv[3:])

    print('Done.')


if __name__ == "__main__":
    main()