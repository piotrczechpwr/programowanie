#!/usr/bin/env python3

from lib.string_utils import StringUtils

def main():
    password = StringUtils.generate_password(min_digits=2, min_special_chars=2)

    print(f'Wygenerowane hasło: "{password}"')

if __name__ == "__main__":
    main()