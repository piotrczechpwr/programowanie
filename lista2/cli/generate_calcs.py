#!/usr/bin/env python3

import sys

from lib.math_utils import MathUtils

def main():

    if (len(sys.argv) != 3):
        print("You have to provide calcus in format numberXnumber, where X is only '+', '-' or '*' sign.")
        return

    operation_text = sys.argv[2]

    MathUtils.drawOnScreenOperation(operation_text)

    print('Done.')

if __name__ == "__main__":
    main()