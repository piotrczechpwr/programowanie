import sys

from lib.image_utils import ImageUtils

def byText():
    if (len(sys.argv) < 4):
        print("You have to provide path to an image path and watermark text.")
        return

    img_path = sys.argv[2]
    watermark_text = sys.argv[3]
    ImageUtils.createTextWatermark(file_path=img_path, text=watermark_text)

    print('Done.')

def byImage():

    if (len(sys.argv) < 4):
        print("You have to provide path to an image path and watermark image path.")
        return

    img_path = sys.argv[2]
    watermark_path = sys.argv[3]
    ImageUtils.createImageWatermark(file_path=img_path, watermark_path=watermark_path)

    print('Done.')