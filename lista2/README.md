# Lista 2

## Uruchamianie
```bash
./main.py [nazwa-komendy]
```

lub 

```bash
python3 ./main.py [nazwa-komendy]
```

## Lista dostępnych komend i ich parametrów

### ./main.py password

```bash
./main.py password
./main.py p
```

Funkcjonalność z wskazanego podpunktu 1.<br>
Nie posiada argumentów wprowadzanych z konsoli. <br>
Globalne ustawienie na generowanie hasła o długości 8 znaków. <br>
Jeśli istnieje potrzeba zmiany minimalnej liczby liter, cyfr lub znaków <br>
- jest to możliwe przy konfiguracji uruchamiania funkcji w pliku `cli/generate_password.py`. <br><br>

Linię:
```python
    password = StringUtils.generate_password(min_digits=2, min_letters=2, min_special_chars=2)

    # np.
    password = StringUtils.generate_password(min_digits=2, min_special_chars=2)
    password = StringUtils.generate_password(min_special_chars=5)
    password = StringUtils.generate_password(min_digits=2, min_special_chars=2)
    password = StringUtils.generate_password(min_special_chars=2, min_letters=3)
    password = StringUtils.generate_password()
```
możemy dowolnie modyfikować, do momentu kiedy nasze dane spełniają odpowiednie formy. <br>

### ./main.py generate_thumbnail

```bash
./main.py generate_thumbnail [--image-path]
./main.py t [--image-path]

# Przykładowe uruchomienie
./main.py t ./test.png
./main.py generate_thumbnail /home/user/lista2/test.jpg
```

Funkcjonalność z wskazanego podpunktu 2.<br>
Posiada jeden wymagany argument z konsoli - scieżkę do pliku względną lub bezwględną.<br>
Akceptuje jpg, jpeg i png. <br>
Jeśli istnieje potrzeba zmiany wielkości lub lokalizacji pluku wyjściowego <br>
- jest to możliwe przy konfiguracji uruchamiania funkcji w pliku `cli/generate_thumbnail.py`. <br><br>

Linię:
```python
    ImageUtils.createThumbnail(file_path=img_path, thumbnail_height=256, thumbnail_width=256)

    # np.
    ImageUtils.createThumbnail(file_path=img_path, thumbnail_height=64, thumbnail_width=64)
    ImageUtils.createThumbnail(file_path=img_path, thumbnail_name="test.png")
```

możemy dowolnie modyfikować, do momentu kiedy nasze dane spełniają odpowiednie formy. <br>

### ./main.py zip

```bash
./main.py zip [--output-path] [**files&directories]
./main.py z [--output-path] [**files&directories] 

# Przykładowe uruchomienie
./main.py zip archiwum ./assets/ ./cli ./README.md 
```

Funkcjonalność z wskazanego podpunktu 3.<br>
Posiada dwa wymagane argumenty z konsoli - scieżkę do archiwum wyjściowego względną lub bezwględną.<br>
Oraz co najmniej 1 plik/folder, natomiast istnieje opcja podawania w nieskończoność.

### ./main.py pdf

```bash
./main.py pdf [--pdf-path] [**length_of_parts]
./main.py d [--pdf-path] [**length_of_part] 

# Przykładowe uruchomienie
./main.py pdf ./sample.pdf 2 1 2
./main.py pdf ./sample.pdf 4
./main.py pdf ./sample.pdf 1 1
```

Funkcjonalność z wskazanego podpunktu 4.<br>
Posiada dwa wymagane argumenty z konsoli - scieżkę do pdf wejściowego względną lub bezwględną.<br>
Oraz co najmniej 1 liczbę stron nowej części PDF-a. <br>
Podawane liczby są kolejnymi liczbami stron kolejnych części. <br> 
Jeśli ich suma będzie mniejsza niż długość PDF-a, automatycznie utworzy się ostatnia część z pozostałymi.

### ./main.py watermark_text

```bash
./main.py watermark_text [--img-path] [--watermark-text]
./main.py wt [--img-path] [--watermark-text] 

# Przykładowe uruchomienie
./main.py watermark_text ./test.png "pWr TeStt"
./main.py wt /home/user/lista2/test.jpg "alamakota"
```

Funkcjonalność z wskazanego podpunktu 5.<br>
Posiada dwa wymagane argumenty z konsoli - scieżkę do obrazku wejściowego względną lub bezwględną.<br>
Oraz tekst, który zostanie wprowadzony jako obraz wodny.<br>
Akceptuje jpg, jpeg i png. <br>

### ./main.py watermark_image

```bash
./main.py watermark_image [--img-path] [--watermark-path]
./main.py wi [--img-path] [--watermark-path] 

# Przykładowe uruchomienie
./main.py watermark_image ./test.png ./test2.jpg
./main.py wi /home/user/lista2/test.jpg ./test3.png
```

Funkcjonalność z wskazanego podpunktu 5.<br>
Posiada dwa wymagane argumenty z konsoli - scieżkę do obrazku wejściowego względną lub bezwględną.<br>
Oraz ścieżkę, która zostanie wprowadzony jako obraz wodny.<br>
Akceptuje jpg, jpeg i png. <br>
Plik obrazu wodnego musi być mniejszy na szerokość i wysokość niż plik główny.

### ./main.py maths

```bash
./main.py maths [--img-path] [--watermark-path]
./main.py m [--img-path] [--watermark-path] 

# Przykładowe uruchomienie
./main.py maths 12-259
./main.py m "2*8"
./main.py m 555+45
```

Funkcjonalność z wskazanego podpunktu 6.<br>
Posiada jeden wymagany argument z konsoli - działanie podane w formie łancucha.<br>
Jeśli chcemy wprowadzić operację mnożenia ważne jest użycie cudzysłowiów. <br>
Zabrione są operacje typu `+1+1+1+` czy `1+1+1`