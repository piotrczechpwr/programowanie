from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from lib.file_utils import FileUtils
import os

class ImageUtils:

    def createThumbnail(file_path: str, thumbnail_width: int = 128, thumbnail_height: int = 128, thumbnail_name: str = None) -> None:
        """
        Generuje miniature o zadanej wielkości i nazwie.
        Przyjmowane typy plików to png, jpg i jpeg. Końcowa miniatura domyślnie będzie w formacie .jpg

        :param file_path: ścieżka pliku wejściowego absolutna lub bezwględna
        :param thumbnail_width: szerokość miniatury
        :param thumbnail_height: wysokość miniatury
        :param thumbnail_name: nazwa miniatury
        """
        img_abs_path, filename, _ = ImageUtils._getValidImageData(file_path)

        output_filename = ImageUtils._getCorrectFullImageName(filename, thumbnail_name)

        thumbnail_filename, thumbnail_ext = os.path.splitext(output_filename)

        thumbnail = ImageUtils._generateThumbnail(img_abs_path, thumbnail_width, thumbnail_height)

        ImageUtils._saveThumbnail(thumbnail, thumbnail_filename, thumbnail_ext)
    

    def createTextWatermark(file_path: str, text: str):
        """
        Tworzy znak wodny na podstawie przekazanego łańcucha znaków.

        :param file_path: ścieżka pliku wejściowego absolutna lub bezwględna
        :param text: napis, który zostanie umieszczony na obrazie
        """
        img_abs_path, _, ext = ImageUtils._getValidImageData(file_path)

        if (os.path.exists(f'{img_abs_path}_text_watermark.png')):
            raise ValueError('Cannot save file.')

        image_proxy = Image.open(img_abs_path)
        watermark_image = image_proxy.copy()

        draw = ImageDraw.Draw(watermark_image)
        x, y, font_type = ImageUtils._getTextAssets(image_proxy)

        # Outline
        draw.text((x+1, y), text, fill="yellow", font=font_type)
        draw.text((x-1, y), text, fill="yellow", font=font_type)
        draw.text((x, y+1), text, fill="yellow", font=font_type)
        draw.text((x, y-1), text, fill="yellow", font=font_type)

        # Text
        draw.text((x, y), text, fill="green", font=font_type)

        return watermark_image.save(f'{img_abs_path}_text_watermark{ext}')


    def createImageWatermark(file_path: str, watermark_path: str):
        """
        Tworzy znak wodny na podstawie przekazanego obrazu.

        :param file_path: ścieżka pliku wejściowego absolutna lub bezwględna
        :param text: napis, który zostanie umieszczony na obrazie
        """
        main_abs_path, _, main_ext = ImageUtils._getValidImageData(file_path)
        watermark_abs_path, _, __ = ImageUtils._getValidImageData(watermark_path)

        if (os.path.exists(f'{main_abs_path}_text_watermark.png')):
            raise ValueError('Cannot save file.')

        image_proxy = Image.open(main_abs_path)
        watermark_image = Image.open(watermark_abs_path)

        main_w, main_h = image_proxy.size
        watermark_w, watermark_h = watermark_image.size

        x, y = int(main_w * 0.1), int(main_h * 0.1)

        if (((watermark_w + x) >= main_w) or ((watermark_h + y) >= main_h)):
            raise ValueError('Watermark image should be smaller (width and height!) than provided main image. Consider using create thumbnail util.')

        image_proxy.paste(watermark_image, (x, y))
        
        return image_proxy.save(f'{main_abs_path}_img_watermark{main_ext}')


    def _getTextAssets(image: Image):
        """
        Generuje podstawowe położenie tekstu i przygotuwuje czcionke do nałożenia na obraz.

        :param image: obraz, na który chcemy nakładać tekst
        """
        w, h = image.size
        x, y = int(w * 0.05), int(h * 0.05)

        font_path = os.path.abspath("./assets/YesevaOne.ttf")
        if not (os.path.isfile(font_path)):
            raise LookupError('Font asset not found.')

        font_type = ImageFont.truetype(font_path, 64)

        return x, y, font_type


    def _getValidImageData(file_path: str) -> tuple([str, str, str]):
        """
        Konwertuje ścieżkę na absolutną, uzyskuje nazwę pliku i rozszerzenie.
        Przy okazji sprawdza czy wszystko istnieje

        :param file_path: ścieżka pliku wejściowego absolutna lub bezwględna
        """
        img_abs_path = FileUtils.getAbsPath(file_path)

        if not (os.path.isfile(img_abs_path)):
            raise ValueError('File not found.')
        
        filename, ext = os.path.splitext(img_abs_path)

        if not (FileUtils.isImage(ext)):
            raise ValueError('Unsupported file extension. Please use .PNG, .JPG OR .JPEG')

        return img_abs_path, filename, ext

    def _getCorrectFullImageName(filename: str, thumbnail_filename: str) -> str:
        """
        Sprawdza, czy została wprowadzona nazwa pliku wyjściowego, jeśli nie to tworzy domyślną:
        nazwa_pliku_wejsciowego + _thumbnail

        :param filname: rozszerzenie pliku poprzedzone kropką.
        :param thumbnail_filename: rozszerzenie pliku poprzedzone kropką.
        """

        if (thumbnail_filename is None):
            return filename + '_thumbnail.jpeg'

        thumbnail_path = FileUtils.getAbsPath(thumbnail_filename)

        return thumbnail_path


    def _generateThumbnail(path: str, width: int, height: int) -> Image:
        """
        Generuje i zwraca obiekt Image, który jest już miniaturą.

        :param path: ścieżka do obrazu.
        :param width: szerokość miniatury.
        :param height: wysokość miniatury.
        """
        with Image.open(path) as im:
            im_rgb = im.convert('RGB')

            im_rgb.thumbnail((width, height))

            return im_rgb


    def _saveThumbnail(im: Image, name: str, ext: str) -> None:
        """
        Zapisuje miniaturę z podaną nazwą w bieżącej lokalizacji.

        :param im: Obiekt Image miniatury.
        :param name: Nazwa wyjściowa.
        :param ext: Format wyjściowy png/jpeg.
        """

        save_path = name + ext

        file_type = 'JPEG'

        if (ext == '.png'):
            file_type = 'PNG'

        if (os.path.isfile(save_path)):
            raise ValueError('Output file cannot be created. File in this directory already exists.')

        return im.save(save_path, file_type)

