import os
from PyPDF2 import PdfReader, PdfWriter
from lib.file_utils import FileUtils

class PdfUtils:

    def divide_pdf(pdf_path: str, wanted_pages: list):
        """
        Z podanego pliku PDF, tworzy mniejsze pliki PDF zgodnie z podziałem podanym przez użytkownika.
        Podział uż polega na kolejnym podaniu oczekiwanych długości plików PDF.
        Jeśli podany podział nie wyczerpuje całości PDF, zostanie automatycznie utworzony jeszcze jeden plik podziału z resztą stron.

        :param pdf_path: ścieżka relatywna/absolutna wejściowego PDF.
        :param wanted_pages: oczekiwany schemat podziału podany za pomocą listy,
        gdzie każda liczba powinna być pozytywną liczbą całkowitą oznaczającą oczekiwaną nową długość pliku.
        """
        pdf_name = PdfUtils._getPdfName(pdf_path)
        divide_numbers = PdfUtils._getDividedNumbers(wanted_pages)

        pdf_reader = PdfReader(pdf_path)
        number_of_pages = len(pdf_reader.pages)

        if (sum(divide_numbers) > number_of_pages):
            raise ValueError('Number of expected pages is higher than pdf number of pages.')
        elif (sum(divide_numbers) != number_of_pages):
            divide_numbers.append(number_of_pages - sum(divide_numbers))

        if not (PdfUtils._isSpaceForPdfParts(divide_numbers, pdf_reader)):
            raise ValueError('Cannot generate smaller pdfs, becouse directory that contains filename + _partX.pdf is occupied.')

        current_page = 0

        for divide_number_index in range(len(divide_numbers)):
            divide_number = divide_numbers[divide_number_index]
            pdf_writer = PdfWriter()

            for _ in range(divide_number):
                pdf_writer.add_page(pdf_reader.pages[current_page])
                current_page += 1

            output_filename = f'{pdf_name}_part{divide_number_index + 1}.pdf'

            with open(output_filename, 'wb') as out:
                pdf_writer.write(out)

        return True        
    

    def _getPdfName(path: str) -> str:
        """
        Otrzymaj poprawną nazwę pliku wejściowego i sprawdź czy istnieje.

        :param path: ścieżka relatywna/absolutna wejściowego PDF.
        :return: Absolutna ścieżka do pliku pdf.
        """
        pdf_path_abs = FileUtils.getAbsPath(path)

        if not (os.path.isfile(pdf_path_abs)):
            raise ValueError('Provided file does not exists.')
        
        pdf_name, file_ext = os.path.splitext(pdf_path_abs)
        if (file_ext != '.pdf'):
            raise ValueError('Provided file is not pdf.')
        
        return pdf_name

    
    def _getDividedNumbers(schema_to_divide: list) -> list:
        """
        Odczytaj, sprawdź i przekonwertuj podany schemat podziału przez użytkownika do wykorzystywanej przyz Utilsy formy.

        :param schema_to_divide: lista oczekiwanego podziału.
        :return: Lista liczb dodatnich całkowitych.
        """
        divide_numbers = []

        for arg in schema_to_divide:
            if arg.isdigit():
                if (int(arg) < 1):
                    raise ValueError('Provided expected page must be positive integer.')
                divide_numbers.append(int(arg))
            else:
                raise ValueError('Provided expected page is not an integer')
        
        return divide_numbers


    def _isSpaceForPdfParts(divide_schema: list, name: str) -> bool:
        """
        Plik podziału tworzone są za pomocą schematu:
        nazwagłówna_partX.pdf, gdzie X to nr podziału
        Funkcja sprawdza czy dla każdego takiego pliku jest miejsce na dysku.

        :param divide_schema: lista oczekiwanego podziału.
        :param name: nazwa głównego pliku wejściowego PDF.
        :return: Wartość boolowska, true jeśli miejsce istnieje.
        """
        for number in range(len(divide_schema)):
            if (os.path.isfile(f'{name}_part{number + 1}.pdf')):
                return False
        
        return True