import os
import glob

from zipfile import ZipFile
from lib.file_utils import FileUtils

class ZipUtils:

    def createZip(output_name: str, to_save: list) -> str:
        """
        Tworzy archiwum ZIP o podanej nazwie dla podanych ścieżek do plików/folderów. 

        :param output_name: nazwa (ew. ścieżka) końcowego archiwum ZIP.
        :param to_save: lista ścieżek do zapisania. Dopuszczalne foldery i pliki.
        :return: ścieżka utworzonego ZIP-a.
        """

        output_path = ZipUtils._getOutputZipName(output_name)

        if (os.path.isfile(output_path)):
            raise ValueError('Save path is occupied by another file.')

        items_to_save = ZipUtils._getListOfSavingItems(to_save)

        with ZipFile(output_path, 'w') as zip_proxy:
            for path in items_to_save:
                zip_proxy.write(path)

        return output_path


    def _getOutputZipName(name: str) -> str:
        """
        Stwórz absolutną sciężkę z rozszerzeniem .zip do archiwum.

        :param name: nazwa ścieżki, relatywna lub absolutna.
        :return: ścieżka absolutna + rozszerzenie zip.
        """

        path = FileUtils.getAbsPath(name)
        filename, _ = os.path.splitext(path)

        return filename + '.zip'
    

    def _getListOfSavingItems(list_proxy: list) -> list:
        """
        Sprawdza czy każdy plik/ścieżka został prawidłowo podany i czy istnieje.
        Końcowo usuwa duplikaty.

        :param list_proxy: lista elementów do zapisu.
        :return: Zwraca poprawna listę ścieżek do zapisu w formie bezwględnej.
        """

        to_storage = []

        for path in list_proxy:
            path_abs = FileUtils.getAbsPath(path)

            if not (os.path.exists(path_abs)):
                raise ValueError(f'Provided file to save {path} does not exists.')
            
            if (os.path.isdir(path_abs)):

                # Get all files from folder&subfolders.
                for directory_file in glob.glob(f'{path}/**/*', recursive=True):
                    to_storage.append(os.path.relpath(directory_file))

            elif (os.path.isfile(path_abs)):
                to_storage.append(os.path.relpath(path_abs))

            else:
                raise TypeError('Provided path is not recognized as directory or file.')

        return list(set(to_storage))