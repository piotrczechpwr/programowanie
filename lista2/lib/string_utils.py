from random import choice, shuffle, randint
import string

class StringUtils:

    def generate_password(min_digits: int = 1, min_letters: int = 1, min_special_chars: int = 1) -> str:
        """
        Generuje losowe hasło o zadanej długości, minimalnej liczbie cyfr, liter i znaków specjalnych.

        :param min_digits: minimalna liczba cyfr w haśle
        :param min_letters: minimalna liczba liter w haśle
        :param min_special_chars: minimalna liczba znaków specjalnych w haśle
        :return: losowo wygenerowane hasło
        """

        password_length = 8

        if not isinstance(min_digits, int) and isinstance(min_letters, int) and isinstance(min_special_chars, int):
            raise ValueError('All params should be integers')

        if not (min_digits > 0 and min_letters > 0 and min_special_chars > 0):
            raise ValueError('All params should be positive integers.')

        if (min_digits + min_letters + min_special_chars > 8):
            raise ValueError('Params are too big.')

        exact_digits = randint(min_digits, (password_length - min_letters - min_special_chars))
        exact_letters = randint(min_letters, (password_length - exact_digits - min_special_chars))
        exact_special_chars = password_length - exact_digits - exact_letters

        password = []

        for _ in range(exact_digits):
            password += choice(string.digits)

        for _ in range(exact_letters):
            password += choice(string.ascii_letters)

        for _ in range(exact_special_chars):
            password += choice(string.punctuation)

        shuffle(password)

        password = ''.join(password)

        return password


