import re

class MathUtils:
    
    def drawOnScreenOperation(provided_text: str) -> None:
        """
        Dla działania podanego w formie łańcucha znaków (np. "235+72")
        wygeneruje odpowiadający mu „słupek” z wynikiem, tzn.:
          235
        +  72
        -----
          317

        :param provided_text: podane działanie jako łancuch znaków.
        """
        operation_text = MathUtils._getExpectedText(provided_text)

        number_a, number_b, symbol, result = None, None, None, None

        if ('+' in operation_text):
            number_a, number_b = operation_text.split('+')
            symbol = '+'
            result = int(number_a) + int(number_b)
        elif ('-' in operation_text):
            number_a, number_b = operation_text.split('-')
            symbol = '-'
            result = int(number_a) - int(number_b)
        else:
            number_a, number_b = operation_text.split('*')
            symbol = '*'
            result = int(number_a) * int(number_b)
        
        MathUtils._displayResultOnScreen(number_a=number_a, number_b=number_b, operation=symbol, result=result)
    

    def _getExpectedText(provided_text: str) -> str:
        """
        Sprawdzi i odczyta czy podany został tekst w prawidłowym formacie.
        Tzn. nie ma spacji, istnieje jeden znak itd.

        :param provided_text: podane działanie jako łancuch znaków.
        """
        two_digits_and_operator_regex = "\d+[\+\-\*]\d+"

        operation_texts = re.findall(two_digits_and_operator_regex, provided_text)

        if (len(operation_texts) != 1):
            raise ValueError('Provided string is uncorrect with the expected format.')

        operation_text = operation_texts[0]

        if (operation_text != provided_text):
            raise ValueError('Provided string is uncorrect with the expected format.')

        return operation_text

    
    def _displayResultOnScreen(number_a: int, number_b: int, operation: str, result: int) -> None:
        """
        Wyświetli rezultat operacji na konsoli

        :param number_a: liczba wyświetlona na górze.
        :param number_b: liczba wyświetlona na dole.
        :param operation: symbol operacji.
        :param result: wynik działania.
        """
        line_length = max(len(number_a), len(number_b), len(str(result)))

        print('  ' + (' ' * (line_length - len(str(number_a)))) + str(number_a))
        print(operation + ' ' + (' ' * (line_length - len(str(number_b)))) + str(number_b))
        print('-' * (line_length + 2))
        print('  ' + (' ' * (line_length - len(str(result)))) + str(result))

