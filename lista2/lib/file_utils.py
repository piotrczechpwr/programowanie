import os

class FileUtils:
    def getAbsPath(file_path: str) -> str:
        """
        Zwraca ścieżkę w postaci absolutnej.

        :param file_path: ścieżka pliku wejściowego absolutna lub bezwględna
        """
        if not (os.path.isabs(file_path)):
            return os.path.abspath(file_path)

        return file_path
        

    def isImage(extension: str) -> bool:
        """
        Sprawdza czy rozszerzenie jest obsługiwane przez klasę.

        :param extension: rozszerzenie pliku poprzedzone kropką.
        """
        return (extension in ['.jpg', '.jpeg', '.png'])